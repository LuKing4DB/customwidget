//
//  OTSTabView.m
//  OneStore
//
//  Created by huang jiming on 13-3-11.
//  Copyright (c) 2013年 OneStore. All rights reserved.
//

#import "OTSTabView.h"
@interface OTSTabView()

@property(assign) NSInteger currentIndex;
@property(nonatomic, strong) NSArray *normalImages;
@property(nonatomic, strong) NSArray *selImages;

@property(nonatomic, strong) NSMutableArray* images;
@property(nonatomic, strong) NSMutableArray* labels;
@end

@implementation OTSTabView

- (id)initWithFrame:(CGRect)frame
              texts:(NSArray *)aArray
              color:(UIColor *)aColor
               font:(UIFont *)aFont
       normalImages:(NSArray *)bArray
          selImages:(NSArray *)cArray
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.normalImages = bArray;
        self.selImages = cArray;
        
        int count = aArray.count;
        CGFloat width = frame.size.width/count;
        CGFloat height = frame.size.height;
        CGFloat xValue = 0.0;
        self.labels = [NSMutableArray arrayWithCapacity:count];
        self.images = [NSMutableArray arrayWithCapacity:count];
        
        for (int i=0; i<count; i++) {
            NSString *normalImageName = [bArray objectAtIndex:i];
            NSString *selImageName = [cArray objectAtIndex:i];
            NSString *text= [aArray objectAtIndex:i];
            UIImageView *theIv = [[UIImageView alloc] initWithFrame:CGRectMake(xValue, 0, width, height)];
            theIv.tag = i;
            theIv.userInteractionEnabled = YES;
            [self addSubview:theIv];
            [self.images addObject:theIv];
            
            //手势处理
            UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [theIv addGestureRecognizer:tapGes];
           theIv.userInteractionEnabled = YES;
            
            UILabel *theLbl = [[UILabel alloc] initWithFrame:CGRectMake(xValue, 0, width, height)];
            theLbl.backgroundColor = [UIColor clearColor];
            theLbl.numberOfLines = 2;
            theLbl.text = text;
            theLbl.textColor = aColor;
            theLbl.font = aFont;
            theLbl.textAlignment = NSTextAlignmentCenter;
            [self addSubview:theLbl];
            [self.labels addObject:theLbl];
            
            xValue += width;
            
            if (i == 0) {
                theIv.image = [UIImage imageNamed:selImageName];
            } else {
                theIv.image = [UIImage imageNamed:normalImageName];
            }
        }
    }
    return self;
}

- (void)updateWithTextAry:(NSArray *)aTextAry
{
    if(!aTextAry.count) {
        return;
    }
    
    if(!self.labels) {
        self.labels = [[NSMutableArray alloc] initWithCapacity:aTextAry.count];
    }
    
    int index = 0;
    for(NSString *text in aTextAry) {
        UILabel *label = [self.labels objectAtIndex:index];
        label.text = text;
        index++;
    }
}

- (void)selectIndex:(NSInteger)index
{
//    if(index == self.currentIndex) {
//       return;
//    }
    if(index >= self.images.count) {
        return;
    }
    UIImageView *oldSelectImageView = [self.images objectAtIndex:self.currentIndex];
    UIImageView *currentSelectImageView = [self.images objectAtIndex:index];
    
   oldSelectImageView.image = [UIImage imageNamed:[self.normalImages objectAtIndex:self.currentIndex]];
    currentSelectImageView.image = [UIImage imageNamed:[self.selImages objectAtIndex:index]];
    self.currentIndex = index;
    
    if ([self.delegate respondsToSelector:@selector(tabView:changeToIndex:)]) {
        [self.delegate tabView:self changeToIndex:self.currentIndex];
    }
}

- (void)handleTap:(UIPanGestureRecognizer*)gestureRecognizer
{   
    UIImageView *selIv = (UIImageView *)gestureRecognizer.view;
    NSInteger index = selIv.tag;
    [self selectIndex:index];
}

@end
